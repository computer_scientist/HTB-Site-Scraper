use clap::{Arg, Command};
use regex::Regex;
use reqwest;
use scraper::Html;
use std::collections::HashMap;
use std::error::Error;

fn fetch_html(url: &str) -> Result<String, reqwest::Error> {
    reqwest::blocking::get(url)?.error_for_status()?.text()
}

fn extract_words(html: &str) -> Vec<String> {
    let document = Html::parse_document(html);
    let body = document.root_element();
    let text = body.text().collect::<Vec<_>>().join(" ");

    Regex::new(r"\w+")
        .unwrap()
        .find_iter(&text)
        .map(|word| word.as_str().to_lowercase())
        .collect()
}

fn count_words(words: Vec<String>, min_length: usize) -> HashMap<String, usize> {
    words
        .into_iter()
        .filter(|word| word.len() >= min_length)
        .fold(HashMap::new(), |mut acc, word| {
            *acc.entry(word).or_insert(0) += 1;
            acc
        })
}

fn main() -> Result<(), Box<dyn Error>> {
    let matches = Command::new("Web Word Counter")
        .arg(
            Arg::new("URL")
                .short('u')
                .long("url")
                .help("URL of webpage to extract from.")
                .required(true)
                .value_parser(clap::builder::ValueParser::string()),
        )
        .arg(
            Arg::new("LENGTH")
                .short('l')
                .long("length")
                .help("Minimum word length (default: 0, no limit).")
                .value_parser(clap::builder::ValueParser::string()),
        )
        .get_matches();

    let url = matches.get_one::<String>("URL").expect("URL is required");
    let length: usize = matches
        .get_one::<String>("LENGTH")
        .unwrap_or(&"0".to_string())
        .parse()
        .expect("Invalid length");

    let html = fetch_html(url)?;
    let words = extract_words(&html);
    let word_counts = count_words(words, length);

    let mut word_counts: Vec<_> = word_counts.into_iter().collect();
    word_counts.sort_by(|a, b| b.1.cmp(&a.1));

    for (i, (word, count)) in word_counts.iter().take(10).enumerate() {
        println!("{}. {} ({})", i + 1, word, count);
    }

    Ok(())
}
